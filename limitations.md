# Study limitations

Despite the fact that matching and the multivariable adjustments were performed, we acknowledge all limitations of a single-center retrospective study, which restricts the generalizability of our findings. 

Also, we cannot rule out that some of the so-called cancer-negative patients may have had undiagnosed cancer at the time of ACS. This seems likely, given the observed prevalence of CA of just .9% (just 157 of 17,797 patients), which is starkly lower than reported in literature (e.g., 6.4% in {Iannaccone:2017de}). 

Additionally, due to administrative limitations, data on subsequent hospitalizations at other hospitals were unobtainable, so long-term outcomes are missing. 

Finally, we are also aware of the fact that the cancer group was heterogenous in terms of cancer type, stage of the disease and history of oncological treatment. 

