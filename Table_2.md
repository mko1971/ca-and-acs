| Characteristics                                | Cancer patients (n=157) | Cancer-negative patients (n=157) | *p* value |
| ---------------------------------------------- | :---------------------: | :------------------------------: | :-------: |
| **ACS types**                                  |                         |                                  |           |
| STEMI (total)                                  |        90 (57.0)        |            90 (57.0)             |   1.000   |
| anterior wall                                  |        44 (28.0)        |            44 (28.0)             |   1.000   |
| other                                          |        46 (29.0)        |            46 (29.0)             |   1.000   |
| NSTEMI                                         |        67 (43.0)        |            67 (43.0)             |   1.000   |
|                                                |                         |                                  |           |
| **Cardiovascular risk factors**                |                         |                                  |           |
| Age (y), (mean±SD)                             |          70±11          |              70±12               |   .972    |
| Male sex (No. %)                               |       114 (72.6)        |            114 (72.6)            |   1.000   |
| Smoking history (No. [%])                      |        43 (27.4)        |            54 (34.4)             |   .179    |
| Hypertension (No. [%])                         |        99 (63.1)        |            103 (65.6)            |   .637    |
| Hypercholesterolemia (No. [%])                 |        77 (49.0)        |            101 (64.3)            |   .006    |
| Diabetes mellitus (No. [%])                    |        37 (23.6)        |            40 (25.5)             |   .694    |
|                                                |                         |                                  |           |
| **Comorbidities**                              |                         |                                  |           |
| Heart failure (No. [%])                        |        32 (20.4)        |            22 (14.0)             |   .135    |
| CVA/TIA (No. [%])                              |        16 (10.2)        |             15 (9.6)             |   .850    |
| AF parox. (No. [%])                            |        20 (12.7)        |             7 (4.5)              |   .009    |
| AF persist. (No. [%])                          |        12 (7.6)         |             9 (5.7)              |   .498    |
| Peripheral vascular disease  (No. [%])         |        37 (23.6)        |            37 (23.6)             |   1.000   |
| DVT/PE (No. [%])                               |         4 (2.5)         |             4 (2.5)              |   1.000   |
| Previous PCI (No. [%])                         |        23 (14.6)        |            27 (17.2)             |   .537    |
| Previous MI (No. [%])                          |        34 (21.7)        |            29 (18.5)             |   .481    |
| Previous CABG (No. [%])                        |        13 (12.7)        |             11 (7.0)             |   .089    |
|                                                |                         |                                  |           |
| **Clinical presentation**                      |                         |                                  |           |
| Predominant syndrome                           |                         |                                  |           |
| Typical chest pain (No. [%])                   |       109 (69.4)        |            134 (85.4)            |   .001    |
| Atypical chest pain (No. [%])                  |        31 (19.7)        |             13 (8.3)             |   .003    |
| SCA (No. [%])                                  |        10 (6.4)         |             2 (1.3)              |   .019    |
| Heart failure on admission (No. [%])           |        61 (38.9)        |            31 (19.7)             |   .000    |
| Echocardiography                               |                         |                                  |           |
| LVEF (%) (mean±SD)                             |          43±12          |              47±12               |   .004    |
| LVd (mm) (mean±SD)                             |          51±7           |               51±5               |   .788    |
|                                                |                         |                                  |           |
| **Angiographic presentation**                  |                         |                                  |           |
| D2B<90min (No. [%])                            |       101 (66.4)        |            124 (80.0)            |   .007    |
| Coronarography (No. [%])                       |       152 (96.8)        |            155 (98.7)            |   .251    |
| Coronarography without PCI (No. [%])           |        50 (32.9)        |            24 (15.5)             |   <.001   |
| No. of arteries involved (No. [%])             |                         |                                  |           |
| 0                                              |         2 (1.3)         |             5 (3.2)              |   .263    |
| 1                                              |        68 (44.7)        |            72 (46.5)             |   .763    |
| 2                                              |        37 (24.3)        |            55 (35.5)             |   .033    |
| 3                                              |        45 (29.6)        |            23 (14.8)             |   .002    |
| Coronarography with DES implantation (No. [%]) |        65 (42.8)        |            116 (74.8)            |   <.001   |
| Coronarography with BMS implantation (No. [%]) |        37 (24.3)        |             15 (9.7)             |   <.001   |
| CABG referral (No. [%])                        |        13 (8.6)         |             7 (4.5)              |   .148    |
| TIMI flow 3 in culprit lesion  (No. [%])       |       112 (73.7)        |            141 (91.0)            |   <.001   |

ACS=acute coronary syndrome; AF parox.=paroxysmal atrial fibrillation; AF persis.=persistent atrial fibrillation; BMS=bare metal stent; CABG=coronary artery bypass grafting; CVA=cerebrovascular accident; D2B<90=door-to-balloon time <90min; DES=drug eluting stent; DVT=deep vein thromobosis; LVd=left ventricle in diastole; LVEF=left ventricular ejection fraction; MI=myocardial infarction; NSTEMI=Non-ST-elevation myocardial infarction; PCI=percutaneous coronary intervention; PE=pulmonary embolism; SCA=sudden cardiac arrest; STEMI=ST-elevation myocardial infarction; TIA=transient ischemic attack; TIMI=Thrombolysis In Myocardial Infarction                                                                     

Table 2: Clinical Characteristics of the Study Population (n=157)
