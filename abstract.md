# Abstract

_Background_ We retrospectively investigate the influence of cancer (CA) history on clinical presentation, treatment and angiographic outcomes in patients with acute coronary syndrome (ACS). 

_Methods_ Set as the end point, thrombolysis in myocardial infarction (TIMI) grades of 157 CA patients were compared with those of 157 non-CA patients, matched for age, sex, type of myocardial infarction (ST segment elevation myocardial infarction, STEMI, vs. non-ST segment elevation myocardial infarction, NSTEMI), and localization of the infarction (anterior wall STEMI vs. other wall STEMI). 

_Results_ Patients with CA: (a) more often suffered from three-vessel disease (3VD) (29.6% vs. 14.8%, *p*=.002); 

<!--
	TODO do we include % of referrals to angio?
-->

(b) less frequently received percutaneous coronary intervention (PCI) with door-to-baloon time (D2B) under 90 minutes (66.4% vs. 80.0%, *p*=.007); (c) more frequently underwent coronarography without coronary intervention (32.9% vs. 15.5%, *p*<.001); (d) less often achieved optimal revascularization, i.e., TIMI flow 3 (44.3% vs. 55.7%, *p*<.001).

_Conclusions_ CA history in ACS patients is associated with less frequent achievement of recommended D2B times and poorer blood flow in the infarct-related artery. 
