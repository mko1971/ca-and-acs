# Discussion

**Intro to discussion**

We have found significant differences in clinical presentation, treatment and angiographic outcome of CA patients vs. matched controls. 

**3VD**

Cancer patients more often presented with 3VD (29.6% vs. 14.8%, *p*=.002). This is consistent with the widely-described phenomenon that cancer treatment often leads to advanced atherosclerosis. {Lee:2013ej, Lynch:2010gt, Berliner:2009ix, Gupta:2012js, Schrader:2005vo, Stewart:2010dn} As many as 35% cancer patients (n=55) in our study had received chemotherapy and 8.9% (n=14) undergone chest radiotherapy, while other widely accepted risk factors were similar (see Table 1). Our findings in this respect are consistent with Landes et al. who identified the incidence of 3VD in CA and non-CA groups as, respectively, 50.1% vs. 40.9% with *p*<.001. {Landes:2017ea} Inconclusive results were obtained by both Iannaccone et al. and Kurisu et al.: 48.7% vs. 48.8%, *p*=.96 and 11% vs. 12%, *p*=.8, respectively. {Iannaccone:2017de, Kurisu:2013jg} 

**D2B time**

Another disadvantageous to the CA group result is that a significantly lower percentage of CA patients received angioplasty within the recommended window of D2B<90 minutes (66.4% vs. 80.0%, *p*=.007). A similar parameter, mean symptom-to-ballon-time, was studied by Wang et al. but the results were inconsistent with ours (4.7 h vs. 7.1 h, *p*=.002, for weighted comparisons of the matched cohort). These results, however, are to be taken with caution, as 53.6% of values are reported as missing for this parameter. {Wang:2016hc} Similarly, Kurisu et al. inconclusively reported time from chest pain to coronary angiography (4.9 h vs. 4.0 h, marked as non-significant). {Kurisu:2013jg} 

**Percent referred to coronarography**

According to our data, nearly 100% of both CA and non-CA patients were referred to coronarography (96.8 vs. 98.7, *p*=.251). This practice is far from universal as there are vast differences between medical centers concerning referring cancer patients with ACS to the catheterization laboratory. For example, Landes et al. found that CA patients were referred for urgent PCI more frequently than a cancer-negative group (61.1% vs. 54.4%, *p*=.003). {Landes:2017ea} In a study from MD Anderson Cancer Center, USA, only 3.3% of all cancer patients with ACS received catheter-based revascularization but due to the objective of the study, numbers for the non-CA population were not reported. {Yusuf:2012jx} 

**Percent coronarography without PCI**

In our study, the cancer group received PCI significantly less often than the CA-negative group (68.2% vs. 84.7%, *p*<.001). 

**BRAK INFO W TABELCE O WYNIKACH Z INNYCH PUBLIKACJI**

**Percent achieving TIMI 3**

As we have found, CA patients less often achieved optimal revascularization, i.e., TIMI grade 3 (44.3% vs. 55.7%, *p*<.001). Our observations were confirmed by Iannaccone et al. in their multi-center observational study. They showed that complete revascularization (defined as final angiography result without coronary stenosis >70% in major epicardial vessels or stenosis >50% in the left main artery) was achieved less frequently in patients with malignancy (55.4% vs. 60.3%, *p*=.02). {Iannaccone:2017de} Landes et al. also reported a slightly less complete revascularizatoion in the CA population although the results were non-significant (*p*=.08). {Landes:2017ea} These results were not contradicted by either Wang et al. or Kurisu et al. The percentages of CA vs. non-CA patients who achieved  early reperfusion with the TIMI 3 flow grade were, respectively, 88.6% vs. 91.9%, *p*=.07 and 89% vs. 95%, *p*=.867. {Wang:2016hc, Kurisu:2013jg}

