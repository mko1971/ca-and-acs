| Malignancy type                      | Patients (No. [%]) |
| ------------------------------------ | :----------------: |
| Lung                                 |     32 (20.4%)     |
|    Genitourinary                     |                    |
|    Prostate                          |     22 (14.0%)     |
|    Bladder                           |     11 (7.0%)      |
|    Renal                             |     12 (7.6%)      |
| Hematologic                          |                    |
|    Leukemia/myelodysplastic syndrome |     14 (8.9%)      |
|    Lymphoma                          |      5 (3.2%)      |
| Gastrointestinal                     |                    |
|    Colon                             |     14 (8.9%)      |
|    Rectal                            |      4 (2.5%)      |
|    Gastric                           |      2 (1.3%)      |
|    Esophageal                        |      1 (1.0%)      |
|    Liver                             |      1 (1.0%)      |
|    Pancreas                          |      1 (1.0%)      |
| Gynecologic                          |                    |
|    Breast                            |      7 (4.5%)      |
|    Uterine                           |      5 (3.2%)      |
|    Cervical                          |      2 (1.3%)      |
| Ear, nose and throat                 |                    |
|    Head and neck                     |      9 (5.7%)      |
|    Thyroid                           |      2 (1.3%)      |
| Sarcoma                              |      1 (1.0%)      |
| Central nervous system               |      2 (1.3%)      |
| Unknown primary                      |      1 (1.0%)      |
| Others (melanoma, eye cancer)        |      9 (5.7%)      |
|                                      |                    |
| **Cancer status**                    |                    |
|    Advanced                          |     54 (34.4%)     |
|    Limited                           |    103 (65.6%)     |
|                                      |                    |
| **Cancer treatment**                 |                    |
|    Surgery                           |     74 (47.1%)     |
|    Chemotherapy                      |     55 (35.0%)     |
|    Hormonotherapy                    |      6 (3.8%)      |
|    Radiotherapy                      |     40 (25.5%)     |
|       Chest radiotherapy             |     14 (8.9%)      |

Table 1: Malignancy type distribution in the cancer group (n=157)