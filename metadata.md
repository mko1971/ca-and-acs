---
title:	Cancer history in patients with acute coronary syndrome is associated with less frequent achievement of TIMI flow 3  
author:	Anna Skrobisz et al.  
date:	2017-09-27  
Base Header Level:	2  
...

