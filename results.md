# Results

## Characteristics of the study population and clinical presentation

Table 1 contains malignancy type distribution in the CA group, while Table 2 compares clinical characteristics of both CA and non-CA groups. 

Cardiovascular risk factors and comorbidities were similar in both groups, including history of ischemic heart disease. Notable differences were: less frequent presence of hypercholesterolemia (49.0% vs. 64.3%, *p*=.006) and more prevalent incidents of paroxysmal atrial fibrillation (12.7% vs. 4.5%, *p*=.009) in the CA group. 

In contrast, clinical presentation on admission was markedly different. CA patients more often presented with atypical chest pain (19.7% vs. 8.3%, *p*=.003), sudden cardiac arrest (6.4% vs. 1.3%, *p*=.019), had a higher incidence of heart failure (38.9% vs. 19.7%, *p*<.001) on admission and a slightly lower left ventricular ejection fraction (43% vs. 47%, *p*=.004). 

{{Table_1.md}} 

{{Table_2.md}} 

## Angiographic findings, treatment and clinical outcomes

Patients with CA: (a) more often suffered from 3VD (29.6% vs. 14.8%, *p*=.002); (b) less frequently received percutaneous coronary intervention (PCI) with door-to-baloon time (D2B) under 90 minutes (66.4% vs. 80.0%, *p*=.007); (c) more frequently underwent coronarography without coronary intervention (32.9% vs. 15.5%, *p*<.001); (d) less often achieved optimal revascularization, i.e., TIMI flow 3 (44.3% vs. 55.7%, *p*<.001). Figure 1 summarizes these findings, while Figure 2 presents a detailed distribution of TIMI flow outcomes in CA vs. non-CA patients. 

Additionally, we have established that CA patients with STEMI achieved D2B<90min significantly less frequently than non-CA patients (76.1% vs. 91.0%; *p*=.0075). Analogous results were obtained for NSTEMI patients but they have not reached statistical significance (52.3% vs. 64.2%; *p*=.16663). 

![Figure 1. Key study results](Figure_1.png)

![Figure 2. Distribution of TIMI scores in CA vs. non-CA patients](Figure_2.png)

![Figure 3. Frequency of D2B<90 minutes in CA vs. non-CA patients with STEMI and NSTEMI](Figure_3.png)
