# Methods

## Study settings, participants and design

The following is a retrospective study based on medical record analysis of all patients hospitalized from January 2009 through December 2016 in the Institute of Cardiology in Warsaw, Poland, with STEMI or NSTEMI. With approval of Institute’s ethics board, medical history and treatment, data were extracted from patients' records. 

Out of all 17,797 patients, we have identified 157 (.9%) with a history of active CA, who were randomly matched 1:1 to CA-negative group based on age (per 10-year spans), sex, type of myocardial infarction (STEMI vs. NSTEMI), and localization of the infarction (anterior wall STEMI vs. other wall STEMI). Both groups were compared in terms of clinical characteristics and angiographic findings (i.e., TIMI flow). {TheTIMIStudyGroup:1985em}

CA was defined as either a condition diagnosed within 5 years from admission to the hospital or as an oncological treatment (surgical, chemotherapy, radiotherapy or hormonotherapy) within 5 years from admission. Advanced CA was defined as stage >T2 and/or N1 and/or M1; or any malignancy considered refractory or recurrent. Patients with limited nonmelanoma skin tumors were excluded. 

We are concentrating differences in both groups of the prevalences of: (a) 3VD, (b) PCI within recommended D2B time of <90 minutes and (c) PCI without coronary intervention. 

Following cardiovascular guidelines, primary PCI is the preferred reperfusion strategy in patients with ACS. 

As Lenderink demonstrated that patients with TIMI grade 2 flow had mortality rates similar to those with TIMI flow grades 0 and 1, while prognosis was better in patients with TIMI flow grade 3. {Lenderink:1995wo} 

We defined the end point as (d) the achievement of optimal TIMI flow of 3. 

Consequently, we adopt TIMI grade 3 as a satisfactory result of revascularization treatment. 

## Statistical Analyses

Continuous variables with normal distribution are expressed as a mean ± a standard deviation (SD). Categorical variables are shown as frequencies and percentages. Non-normal distribution variables are expressed as median values with interquartile range. Continuous variable values are compared between groups by the unpaired or paired t test (for Gaussian distribution) and the Mann–Whitney U test (for non-normal distribution). A *p* value of <.05 is denoted as statistically significant. Statistical analyses were performed with Statistica 9.0.