# Conclusions (DRAFT)

1. Conclusions from *our* research 
	
	(the *meaning* of differences BETWEEN the two groups WITHIN one center)

	Any delay in receiving angioplasty is unfortunate, as in patients with confirmed ACS of very high risk, immediate coronary angiography is recommended. {Roffi:2016ej} Additionally, Yudi et al. showed that D2B time less than 90 min. was associated with improved short- and long-term survival in both high and low-risk STEMI patients. {Yudi:2016fs} This is corroborated by Chen et al. who demonstrated increased therapeutic efficacy of even shorter D2B times of 60 minutes. {Chen:2017gp} 
	
	that patinets with concommitant CA were referred to the catheterization laboratory later and more frequently underwent coronarography without coronary intervention than matched controls, even though upon clinical presentation their condition was more serious. Finally, CA patients less often than the controls achieved optimal revascularization. 

	Results from our study suggest that the cancer population can be safely carried through ACS. Pending development of targeted guidelines, it seems reasonable to treat those patients according to current cardiovascular guidelines, in particular striving to ensure achieving recommended time to revascularization. [???]

2. Conclusions from comparison with other results

	1. 3VD – universal agreement that worse in CA
	2. D2B time / similar measures – conflicting results
	3. Percent referred to coro – wide differences [not officially listed]
	4. Percent coro w/o PCI – ????
	5. Percent achieving TIMI 3 – universal agreement that worse in CA

Ad 2.3. Guddati et al., who analyzed the Nationwide Inpatient Sample Database (2000-2009) in terms of outcomes data regarding PCI in metastatic patients, enumerated a list of factors influencing the odds of PCI in a US cancer population. Female sex, advanced age, Afro-American and Hispanic ethnicity, geographical localization of the hospital and lung or pancreatic cancer significantly lowered the odds of receiving PCI. {Guddati:2015fi}
	
Summary of conclusions from comparison with other results:

- Patients arrive in a worse condition
- Are treated differently (in an between) centers, even though well-established general guidelines exist
- In spite of differences in treatment, outcomes are similarly different as measured by TIMI 3

However, there are no universally accepted guidelines targeted at patients with concommitant CA. 