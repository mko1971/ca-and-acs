# Introduction

Coronary heart disease and CA are two major causes of mortality worldwide. {WorldHealthOrganization:2015vh} Demographic data shows that one in ten patients with myocardial infarction (MI) has a history of CA. {Wang:2016hc} Moreover, it is known that in patients treated with PCI a history of CA is a significant independent predictor of an ACS event within 1 year after PCI. {Abbott:2007ep} 

However, most trials in the cardiovascular field exclude patients with CA and only limited data are available from observational studies regarding outcomes of CA patients with ACS. {Kurisu:2013jg, Yusuf:2001wl, Wallentin:2009ev, Wiviott:2007ur} Also, CA history is usually lacking in PCI registries. Currently available data characterizing this special group come from a small number of retrospective studies. {Yusuf:2011jx, Wang:2016hc, Landes:2017ea, Iannaccone:2017de} 

